import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'product', price: 10 },
  { id: 2, name: 'product', price: 20 },
  { id: 3, name: 'product', price: 30 },
];
let lastProductId = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user ' + JSON.stringify(users[index]));
    // console.log('update ' + JSON.stringify(updateUserDto));
    const updateUser: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteuser = products[index];
    products.splice(index, 1);
    return deleteuser;
  }

  reset() {
    products = [
      { id: 1, name: 'product', price: 10 },
      { id: 2, name: 'product', price: 20 },
      { id: 3, name: 'product', price: 30 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
